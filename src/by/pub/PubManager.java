package by.pub;

import by.pub.entity.Pub;
import by.pub.menu.MenuDisplay;
import by.pub.menu.RootMenuItem;

public class PubManager {
    private RootMenuItem rootMenu;

    public PubManager(Pub pub) {
        rootMenu = new MenuDisplay(pub);
    }

    public void execute() {
        rootMenu.execute();
    }
}