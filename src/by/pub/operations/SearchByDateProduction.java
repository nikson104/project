package by.pub.operations;

import by.pub.entity.Good;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SearchByDateProduction implements Action {

    @Override
    public List<Good> execute(List<Good> goods, Object param) {
        if (!(param instanceof Date)) {
            throw new IllegalArgumentException();
        }
        Date year = (Date) param;
        List<Good> filteredList = new ArrayList<>();
        for (Good good : goods) {
            if (good.getYear().equals(year)) ;
            filteredList.add(good);
        }
        return filteredList;
    }

    @Override
    public String name() {
        return null;
    }
}