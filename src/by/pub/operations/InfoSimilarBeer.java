package by.pub.operations;

import by.pub.entity.Beer;
import by.pub.entity.Good;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class InfoSimilarBeer implements Action {

    @Override
    public List<Good> execute(List<Good> goods, Object param) throws InputMismatchException {
        if (!(param instanceof Beer)) {
            throw new InputMismatchException();
        }
        String selectedCountry = ((Beer) param).getCountry();
        List<Good> resultList = new ArrayList();
        for (Good good : goods) {
            if (selectedCountry.equals(good.getCountry())) {
                resultList.add(good);
            }
        }
        return resultList;
    }

    @Override
    public String name() {
        return null;
    }
}