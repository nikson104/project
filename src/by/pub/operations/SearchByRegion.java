package by.pub.operations;

import by.pub.entity.Good;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class SearchByRegion implements Action {

    @Override
    public List<Good> execute(List<Good> goods, Object param) throws InputMismatchException {
        if (!(param instanceof String)) {
            throw new InputMismatchException();
        }
        String region = (String) param;
        List<Good> filteredList = new ArrayList<>();
        for (Good good : goods) {
            if (good.getCountry().equals(region))
            filteredList.add(good);
        }
        return filteredList;
    }

    @Override
    public String name() {
        return null;
    }
}