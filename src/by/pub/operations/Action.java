package by.pub.operations;

import by.pub.entity.Good;

import java.util.InputMismatchException;
import java.util.List;

public interface Action {
    List<Good> execute(List<Good> goods, Object param) throws InputMismatchException;
    String name();
}