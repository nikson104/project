package by.pub.operations;

import by.pub.entity.Good;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class SearchByName implements Action {

    @Override
    public List<Good> execute(List<Good> goods, Object param) throws InputMismatchException {
        if (!(param instanceof String)) {
            throw new InputMismatchException();
        }
        String name = (String) param;
        List<Good> filteredList = new ArrayList<>();
        for (Good good : goods) {
            if (good.getName().equals(name))
            filteredList.add(good);
        }
        return filteredList;
    }

    @Override
    public String name() {
        return null;
    }
}