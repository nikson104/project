package by.pub.operations;

import by.pub.entity.Good;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class SearchByPrice implements Action {

    @Override
    public List<Good> execute(List<Good> goods, Object param)throws InputMismatchException {
        if (!(param instanceof Integer)) {
            throw new InputMismatchException();
        }
        int price = (int) param;
        List<Good> filteredList = new ArrayList<>();
        for (Good good : goods) {
            if (good.getPrice() == price)
            filteredList.add(good);
        }
        return filteredList;
    }

    @Override
    public String name() {
        return null;
    }
}