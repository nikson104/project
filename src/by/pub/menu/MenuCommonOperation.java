package by.pub.menu;

import by.pub.operations.Action;
import by.pub.entity.Good;
import by.pub.entity.Pub;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class MenuCommonOperation {
    private static final Logger LOGGER = Logger.getLogger(MenuCommonOperation.class.getName());

    protected static final Scanner SCANNER = new Scanner(System.in);

    protected Action operation;
    protected RootMenuItem rootMenuItem;
    protected Pub pub;

    public MenuCommonOperation(Action operation, RootMenuItem rootMenuItem, Pub pub) {
        this.rootMenuItem = rootMenuItem;
        this.operation = operation;
        this.pub = pub;
    }

    public void execute(Object param) {
        try {
            List<Good> resultList = operation.execute(pub.getGoods(), param);
            System.out.println("Result: " + resultList);
        } catch (Exception e){
            LOGGER.log(Level.INFO, e.getMessage(),e);
        }
    }
}