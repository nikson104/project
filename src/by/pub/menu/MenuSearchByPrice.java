package by.pub.menu;

import by.pub.operations.SearchByPrice;
import by.pub.entity.Pub;

public class MenuSearchByPrice extends MenuCommonOperation implements MenuItem {
    public MenuSearchByPrice(RootMenuItem rootMenu, Pub pub) {
        super(new SearchByPrice(), rootMenu, pub);

    }

    @Override
    public void execute() {
        System.out.println("Please input beer price:");
        int price = SCANNER.nextInt();
        super.execute(price);
    }


    @Override
    public String name() {

        return "3. Search by beer price";
    }
}
