package by.pub.menu;

import by.pub.operations.SearchByRegion;
import by.pub.entity.Pub;

public class MenuSearchByRegion extends MenuCommonOperation implements MenuItem {
    public MenuSearchByRegion(RootMenuItem rootMenu, Pub pub) {
        super(new SearchByRegion(), rootMenu, pub);
    }

    @Override
    public void execute() {
        System.out.println("Please input beer region:");
        String region = SCANNER.next();
        super.execute(region);
    }

    @Override
    public String name() {
        return "5. Search by region";
    }

    @Override
    public String toString() {
        return "MenuSearchByRegion" +
                "operation=" + operation;
    }
}