package by.pub.menu;

import by.pub.operations.InfoSimilarBeer;
import by.pub.entity.Good;
import by.pub.entity.Pub;

public class MenuInfoSimilarBeer extends MenuCommonOperation implements MenuItem {
    public MenuInfoSimilarBeer(RootMenuItem rootMenu, Pub pub) {
        super(new InfoSimilarBeer(), rootMenu, pub);
    }


    @Override
    public void execute() {
        System.out.println("Please select your favorite beer number: " + pub.getGoods());
        int beerNumber = SCANNER.nextInt();
        Good favoriteBeer = pub.getGoods().get(beerNumber);
        super.execute(favoriteBeer);
    }

    @Override
    public String name() {
        return "6. Info about similar kind of beer";
    }
}
