package by.pub.menu;

import by.pub.entity.Pub;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuDisplay implements RootMenuItem {
    private static final Scanner SCANNER = new Scanner(System.in);

    private List<MenuItem> subMenus = new ArrayList<>();

    public MenuDisplay(Pub pub) {
        subMenus.add(new MenuSortByName(this, pub));
        subMenus.add(new MenuSortByPrice(this, pub));
        subMenus.add(new MenuSearchByName(this, pub));
        subMenus.add(new MenuSearchByPrice(this, pub));
        subMenus.add(new MenuSearchByDateProduction(this, pub));
        subMenus.add(new MenuSearchByRegion(this, pub));
        subMenus.add(new MenuInfoSimilarBeer(this, pub));

    }

    @Override
    public void execute() {
        for (int i = 0; i < subMenus.size(); i++) {
            System.out.println(subMenus.get(i).name());
        }
        System.out.print("Choose a number of operation: ");
        subMenus.get(SCANNER.nextInt()).execute();
    }
}
