package by.pub.menu;

public interface MenuItem {
    void execute();

    String name();
}