package by.pub.menu;

import by.pub.operations.SortByPrice;
import by.pub.entity.Pub;

public class MenuSortByPrice extends MenuCommonOperation implements MenuItem {
    public MenuSortByPrice(RootMenuItem rootMenu, Pub pub) {
        super(new SortByPrice(), rootMenu, pub);
    }

    @Override
    public void execute() {
        super.execute(null);
    }

    @Override
    public String name() {
        return "1. Sort by price of beer";
    }
}