package by.pub.menu;

import by.pub.operations.SortByName;
import by.pub.entity.Pub;

public class MenuSortByName extends MenuCommonOperation implements MenuItem {
    public MenuSortByName(RootMenuItem rootMenu, Pub pub) {
        super(new SortByName(), rootMenu, pub);
    }

    @Override
    public void execute() {
        super.execute(null);
    }

    @Override
    public String name() {
        return "0. Sort by name of beer";
    }
}