package by.pub.menu;

import by.pub.operations.SearchByName;
import by.pub.entity.Pub;

public class MenuSearchByName extends MenuCommonOperation implements MenuItem {
    public MenuSearchByName(RootMenuItem rootMenu, Pub pub) {
        super(new SearchByName(), rootMenu, pub);
    }

    @Override
    public void execute() {
        System.out.println("Please input beer name:");
        String name = SCANNER.next();
        super.execute(name);
    }

    @Override
    public String name() {
        return "2. Search by name of beer";
    }
}
