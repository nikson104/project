package by.pub.menu;

import by.pub.operations.SearchByDateProduction;
import by.pub.entity.Pub;

public class MenuSearchByDateProduction extends MenuCommonOperation implements MenuItem {
    public MenuSearchByDateProduction(RootMenuItem rootMenu, Pub pub) {
        super(new SearchByDateProduction(), rootMenu, pub);
    }

    @Override
    public void execute() {
        System.out.println("Enter a date production of beer");
        String date = SCANNER.next();
        super.execute(date);
    }

    @Override
    public String name() {
        return "4. Search by date production of beer";
    }

}