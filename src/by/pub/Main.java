package by.pub;

import by.pub.entity.Beer;
import by.pub.entity.Good;
import by.pub.entity.Pub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Pub pub = createPub();
        new PubManager(pub).execute();
    }

    private static Pub createPub() {
        List<String> eMail = new ArrayList<>();
        List<Good> goods = new ArrayList<>();
        eMail.add("sgasdg@gmail.com");
        eMail.add("aaaaasfs@gmail.com");
        eMail.add("bbbesfwef@gmail.com");
        Beer beer1 = new Beer(0, "Beer N1", "DE", new Date(), 45, true);
        Beer beer2 = new Beer(1, "Beer N2", "RU", new Date(), 534, true);
        Beer beer3 = new Beer(2, "Beer N3", "BY", new Date(), 12, true);
        Beer beer4 = new Beer(3, "Beer N4", "CZ", new Date(), 34, true);
        Beer beer5 = new Beer(4, "Beer N5", "AU", new Date(), 54, true);
        goods.add(beer1);
        goods.add(beer2);
        goods.add(beer3);
        goods.add(beer4);
        goods.add(beer5);
        Pub pub = null;
        pub = new Pub("Super pub", "Minsk", eMail, goods);
        return pub;
    }
}